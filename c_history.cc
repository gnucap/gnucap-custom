/*                            -*- C++ -*-
 * Copyright (C) 2018 Felix Salfelder
 * Author: Felix
 *
 * This file is part of "Gnucap", the Gnu Circuit Analysis Package
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#include <c_comand.h>
#include <ap.h>
#include <globals.h>
#include <fstream>
#include <e_cardlist.h>
#ifdef HAVE_LIBREADLINE
#include <readline/history.h>
static bool use_readline=true;
#else
static bool use_readline=false;
#endif
/*--------------------------------------------------------------------------*/
namespace{
/*--------------------------------------------------------------------------*/
class CMD_HIST : public CMD {
public:
	void do_it(CS& cmd, CARD_LIST*) {
		if (cmd.umatch("on")) {
			std::string gh = std::string(getenv("HOME")) + "/.gnucap_history";

			CMD::command("history < " + gh, &CARD_LIST::card_list);
			CMD::command("log >> " + gh, &CARD_LIST::card_list);
		}else if (cmd.umatch("<")) {
			if(use_readline){
				std::string fn;
				cmd >> fn;
				trace1("rhf", fn);

				char str[BUFLEN];
				std::ifstream file;
				file.open(fn.c_str());
				if( file ){
					while(file.getline(str, BUFLEN)) {
						add_history(str);
					}
				}else{ untested();
				}
				file.close();
			}else{ untested();
				error(bWARNING, "there is no readline history\n");
			}

		}else{ untested();
			// print history...?
			incomplete();
		}
	}
} p1;
DISPATCHER<CMD>::INSTALL d1(&command_dispatcher, "history", &p1);
/*--------------------------------------------------------------------------*/
}
