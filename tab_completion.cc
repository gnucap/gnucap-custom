/*                -*- C++ -*-
 * Copyright (C) 2018 Felix Salfelder
 * Author: Felix Salfelder <felix@salfelder.org>
 *
 * This file is part of "Gnucap", the Gnu Circuit Analysis Package
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *------------------------------------------------------------------
 * tab completion
 */
#include <cstdio> // sometimes needed by readline?
#include <readline/readline.h>
#include <iostream>
#include <string.h>
#include <io_trace.h>
#include <globals.h>

namespace{
/*--------------------------------------------------------------------------*/
char * cmd_generator(const char *text, int state)
{ untested();
//	static int list_index;
	static size_t len;
//	char *name;
	static DISPATCHER<CMD>::const_iterator ci;

	if (!state) { untested();
//		list_index = 0;
		len = strlen(text);
		ci = command_dispatcher.begin();
	}

	while (ci!=command_dispatcher.end()) { untested();
		char const* n=ci->first.c_str();
		++ci;
		if (strncmp(n, text, len) == 0) { untested();
			return strdup(n);
		} else { untested();
		}
	}
	return NULL;
}
/*--------------------------------------------------------------------------*/
char ** cmd_completion ( char const*text, int /*start*/, int /*end*/)
{ untested();
	rl_attempted_completion_over = 1;
	return rl_completion_matches(text, cmd_generator);
}
/*--------------------------------------------------------------------------*/
struct r{
	r(){
		/* Allow conditional parsing of the ~/.inputrc file. */
	  rl_readline_name = "gnucap";

	  rl_attempted_completion_function = cmd_completion;
	}
} c;
/*--------------------------------------------------------------------------*/
}
